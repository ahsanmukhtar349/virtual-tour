$(document).ready(function() {

    $('.parent').on("click",function(){
      
      $(this).find(".sub-nav").toggle();
      $(this).siblings().find(".sub-nav").hide();
      
      if($(".sub-nav:visible").length === 0 ){
        $("#menu-overlay").hide();
      }else {
        $("#menu-overlay").show();
      }
    });
    
     $("#menu-overlay").on("click",function(){
       $(".sub-nav").hide();
       $(this).hide();
     });
  });

  

  $('.owl-carousel').owlCarousel({
    loop:false,
    margin:10,
    autoplay:false,
    animateOut: 'fadeOut',
    mouseDrag:false,
    nav:true,
    navText : ["<span id='rightarrow' class='ion-chevron-right fa fa-angle-left'></span>","<span id='nextbutton' class='ion-chevron-left fa fa-angle-right stop'>  </span>"],
    dots:false,
    onTranslated:callBack,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1000:{
            items:1
        }
    }
   
});

function callBack(){
  if($('.owl-carousel .owl-item').last().hasClass('active')){
        $('.owl-next').hide();
        $('.owl-prev').show(); 
        console.log('true');
     }else if($('.owl-carousel .owl-item').first().hasClass('active')){
        $('.owl-prev').hide(); 
        $('.owl-next').show();
        console.log('false');
     }
}
$('#owlCarousel .owl-prev').hide();

$('#addspotno').on('click',function(){
  document.getElementById('mainimage').src="images/secondproject.png";
  var model4 = document.getElementById("myModal4");
  modal4.style.display = "none";
 
})

$( "#addspotno" ).click(function() {
  $("#nextbutton").trigger('click');
});

$('#addspotyes').on('click',function(){
  document.getElementById('mainimage').src="images/secondproject.png";
  var model4 = document.getElementById("myModal4");
  modal4.style.display = "none";
})
$( "#addspotyes" ).click(function() {
  $("#nextbutton").trigger('click');
});

$('#rightarrow').on('click',function(){
  document.getElementById('mainimage').src="images/frontimage.png";
})

$( "#rightarrow" ).click(function() {
  document.getElementById("myBtn4").style.display="block";
});

// $('.play').on('click',function(){
//   owl.trigger('play.owl.autoplay',[1000])
// })
// $('.stop').on('click',function(){
//   owl.trigger('stop.owl')
// })

// -------------show a popup map-------------

  // Get the modal
var modal = document.getElementById("myModal");

// Get the button that opens the modal
var btn = document.getElementById("myBtn");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks the button, open the modal 
btn.onclick = function() {
  modal.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
  modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
}

// -------------show a popup map -------------

// -------------show a popup map update-------------

  // Get the modal
  var modal1 = document.getElementById("myModal1");

  // Get the button that opens the modal
  var btn = document.getElementById("myBtn1");
  
  // Get the <span> element that closes the modal
  var span = document.getElementsByClassName("close1")[0];
  
  // When the user clicks the button, open the modal 
  btn.onclick = function() {
    modal1.style.display = "block";
  }
  
  // When the user clicks on <span> (x), close the modal
  span.onclick = function() {
    document.getElementById("myBtn").style.display = "block";
    modal1.style.display = "none";
    
  }
  
  // When the user clicks anywhere outside of the modal, close it
  window.onclick = function(event) {
    if (event.target == modal1) {
      modal1.style.display = "none";
    }
  }
  
  // -------------show a popup map update-------------

// -------------show a popup map upload-------------

  // Get the modal
  var modal2 = document.getElementById("myModal2");

  // Get the button that opens the modal
  var btn = document.getElementById("myBtn2");
  
  // Get the <span> element that closes the modal
  var span = document.getElementsByClassName("close2")[0];
  
  // When the user clicks the button, open the modal 
  btn.onclick = function() {
    modal2.style.display = "block";
  }
  
  // When the user clicks on <span> (x), close the modal
  span.onclick = function() {
    modal2.style.display = "none";
  }
  
  // When the user clicks anywhere outside of the modal, close it
  window.onclick = function(event) {
    if (event.target == modal2) {
      modal2.style.display = "none";
    }
  }
  
  // -------------show a popup map upload-------------

  // -------------add spotinfo popup-------------

  // Get the modal
  var modal3 = document.getElementById("myModal3");

  // Get the button that opens the modal
  var btn = document.getElementById("myBtn3");
  
  // Get the <span> element that closes the modal
  var span = document.getElementsByClassName("close3")[0];
  
  // When the user clicks the button, open the modal 
  btn.onclick = function() {
    modal3.style.display = "block";
    document.getElementById("spotinfo").style.display = "none";
  }
  
  // When the user clicks on <span> (x), close the modal
  // span.onclick = function() {
  //   modal3.style.display = "none";
  // }
  
  // When the user clicks anywhere outside of the modal, close it
  // window.onclick = function(event) {
  //   if (event.target == modal3) {
  //     modal3.style.display = "none";
  //   }
  // }
  
  // -------------add spotinfo popup-------------
  


var elem = document.getElementById("fullview");
function openFullscreen() {
  if (elem.requestFullscreen) {
    document.getElementById("fullview").style.display = "block";
    elem.requestFullscreen();
  } else if (elem.mozRequestFullScreen) { /* Firefox */
    document.getElementById("fullview").style.display = "block";
    elem.mozRequestFullScreen();
  } else if (elem.webkitRequestFullscreen) { /* Chrome, Safari & Opera */
    document.getElementById("fullview").style.display = "block";
    elem.webkitRequestFullscreen();
  } else if (elem.msRequestFullscreen) { /* IE/Edge */
    document.getElementById("fullview").style.display = "block";
    elem.msRequestFullscreen();
  }
}

function closeFullscreen() {
  if (document.exitFullscreen) {
    document.getElementById("fullview").style.display = "none";
    document.exitFullscreen();
  } else if (document.mozCancelFullScreen) {
    document.getElementById("fullview").style.display = "none";
    document.mozCancelFullScreen();
  } else if (document.webkitExitFullscreen) {
    document.getElementById("fullview").style.display = "none";
    document.webkitExitFullscreen();
  } else if (document.msExitFullscreen) {
    document.getElementById("fullview").style.display = "none";
    document.msExitFullscreen();
  }
}

$(document).ready(function(){
  $('#myBtn1').click(function(){
      $('#img_preview').html('<img src="' + $('#myBtn').attr('src') + '" />');
  });
});

$('.deleteimage').click(function(){
  document.getElementById("myBtn").style.display = "none";
  document.getElementById("img_preview").style.display = "none";
  document.getElementById("deleteicon").style.display = "none";
  document.getElementById("damiicon").style.display = "block";
});

$('.rightbutton').click(function(){
  document.getElementById("Infospot").style.display = "block";
  document.getElementById("allbuttons").style.display = "none";
});

$('#Cancel').click(function(){
  document.getElementById("Infospot").style.display = "none";
  document.getElementById("allbuttons").style.display = "block";
  document.getElementById("spotinfo").style.display = "block";
  var modal3 = document.getElementById("myModal3");
  modal3.style.display = "none";
});

$('#Next').click(function(){
  document.getElementById("Previous").style.display = "block";
});
$('#Previous').click(function(){
  document.getElementById("Previous").style.display = "none";
});

// ---------- tabs ---------------
function openCity(evt, cityName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " active";
}

// ---------- tabs ---------------

// for tab display none for next slider
// var owl = $('.owl-carousel');
// owl.owlCarousel();
// // Listen to owl events:
// owl.on('changed.owl.carousel', function(event) {
   
//   document.getElementById("compare").style.display="none";
  
// })

// -------------show a popup for next slider-------------
   var modal4 = document.getElementById("myModal4");

// Get the button that opens the modal
var btn = document.getElementById("myBtn4");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close4")[0];

// When the user clicks the button, open the modal 
btn.onclick = function() {
  modal4.style.display = "block";
  document.getElementById("myBtn4").style.display="none";
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
  modal4.style.display = "none";
  
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
  if (event.target == modal4) {
    modal4.style.display = "none";
  }
}

// -------------show a popup for next slider-------------
var itemCount = jQuery("#owl-demo div").length;
if (itemCount > 1) {
  document.getElementById("rightarrow").show();
  document.getElementById("nextbutton").show();
 
}
else{
  document.getElementById("rightarrow").hide();
  document.getElementById("nextbutton").hide();
  // document.getElementById("menu").style.display="none";
}

 
